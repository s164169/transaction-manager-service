FROM adoptopenjdk:8-jre-hotspot
COPY target/transaction-manager-jar-with-dependencies.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
         -Djava.net.preferIPv4Addresses=true\
        -jar transaction-manager-jar-with-dependencies.jar
