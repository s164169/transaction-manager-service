package com.dtupay.transactionmanager.Adapters;

import com.dtupay.transactionmanager.Abstractions.IBank;
import com.dtupay.transactionmanager.Converters.ConvertAccount;
import com.dtupay.transactionmanager.Converters.ConvertAccountInfo;
import com.dtupay.transactionmanager.Converters.ConvertUser;
import com.dtupay.transactionmanager.Models.DTUPayAccount;
import com.dtupay.transactionmanager.Models.DTUPayAccountInfo;
import com.dtupay.transactionmanager.Models.DTUPayUser;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;

import java.math.BigDecimal;
//@author Mathias Boss Jørgensen
public class SoapBankAdapter implements IBank {
    BankService bank = new BankServiceService().getBankServicePort();
    @Override
    public String createAccountWithBalance(DTUPayUser dtuPayUser, BigDecimal initialBalance) {
        try {
            return bank.createAccountWithBalance(ConvertUser.covertUser(dtuPayUser), initialBalance);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccount getAccount(String accountNumber) {
        try {
            return ConvertAccount.convertAccount(bank.getAccount(accountNumber));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccount getAccountByCPRNumber(String cpr) {
        try {
            return ConvertAccount.convertAccount(bank.getAccountByCprNumber(cpr));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public DTUPayAccountInfo[] getAccounts() {
        return (DTUPayAccountInfo[]) bank.getAccounts().stream().map(ConvertAccountInfo::convertAccountInfo).toArray();
    }

    @Override
    public void retireAccount(String accountNumber) {
        try {
            bank.retireAccount(accountNumber);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void transferMoneyFromTo(String accountFrom, String accountTo, BigDecimal amount, String description) {
        try {
            bank.transferMoneyFromTo(accountFrom,accountTo,amount,description);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }
}
