package com.dtupay.transactionmanager;

import com.dtupay.transactionmanager.Abstractions.ITransactionRequestRepository;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
//@author Sebatian Fischer
public class InMemoryRepository implements ITransactionRequestRepository {
    private Map<UUID, TransactionRequest> transactionRequests = new HashMap<>();
    private Map<String, Token> tokens = new HashMap<>();
    private Map<UUID, Merchant> merchants = new HashMap<>();
    private Map<UUID, Customer> customers = new HashMap<>();

    @Override
    public void addTransaction(TransactionRequest transactionRequest) {
        transactionRequests.put(transactionRequest.getTransactionRequestID(), transactionRequest);
    }

    @Override
    public TransactionRequest getTransactionRequest(UUID transactionRequestID) {
        return transactionRequests.get(transactionRequestID);
    }

    @Override
    public List<TransactionRequest> getAllByMerchantID(UUID merchantID) {
        return transactionRequests.values().stream().filter(x -> x.getMerchant().getUserID() == merchantID).collect(Collectors.toList());
    }

    @Override
    public void updateTransactionRequest(TransactionRequest transactionRequest) {
        transactionRequests.replace(transactionRequest.getTransactionRequestID(), transactionRequest);
    }

    @Override
    public Merchant getMerchantByID(UUID merchantID) {
        return merchants.get(merchantID);
    }

    @Override
    public Customer getCustomerByTokenID(String tokenID) {
        Token token = tokens.get(tokenID);
        return customers.get(token.getCustomerID());
    }

    @Override
    public void addCustomer(Customer customer) {
        customers.put(customer.getUserID(),customer);
    }

    @Override
    public void deleteCustomer(UUID customerID) {
        customers.remove(customerID);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customers.replace(customer.getUserID(),customer);
    }

    @Override
    public void addMerchant(Merchant merchant) {
        merchants.put(merchant.getUserID(), merchant);
    }

    @Override
    public void deleteMerchant(UUID merchantID) {
        merchants.remove(merchantID);
    }

    @Override
    public void updateMerchant(Merchant merchant) {
        merchants.replace(merchant.getUserID(), merchant);
    }

    @Override
    public void addTokens(List<Token> listOfTokens) {
        for (Token token: listOfTokens) {
            tokens.put(token.getTokenID(), token);
        }
    }

    @Override
    public void addToken(Token token) {
        tokens.put(token.getTokenID(), token);
    }
}
