package com.dtupay.transactionmanager.Converters;

import com.dtupay.transactionmanager.Models.DTUPayUser;
import dtu.ws.fastmoney.User;

import java.util.UUID;
//@author Sebatian Fischer
public class ConvertUser {

    public static User covertUser(DTUPayUser dtuPayUser) {
        User user = new User();
        user.setCprNumber(dtuPayUser.getCprNumber());
        user.setFirstName(dtuPayUser.getFirstName());
        user.setLastName(dtuPayUser.getLastName());
        return user;
    }

    public static DTUPayUser revertUser(User user) {
        DTUPayUser dtuPayUser = new DTUPayUser(user.getCprNumber(),user.getFirstName(),user.getLastName(), UUID.randomUUID(), "AccountID");
        return dtuPayUser;
    }
}
