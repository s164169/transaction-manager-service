package com.dtupay.transactionmanager.Converters;

import com.dtupay.transactionmanager.Models.DTUPayTransaction;
import dtu.ws.fastmoney.Transaction;
//@author Frederik Kirkegaard
public class ConvertTransaction {
    public static DTUPayTransaction convertTransaction(Transaction transaction) {
        DTUPayTransaction dtuPayTransaction = new DTUPayTransaction();
        dtuPayTransaction.setAmount(transaction.getAmount());
        dtuPayTransaction.setBalance(transaction.getBalance());
        dtuPayTransaction.setCreditor(transaction.getCreditor());
        dtuPayTransaction.setDebtor(transaction.getDebtor());
        dtuPayTransaction.setDescription(transaction.getDescription());
        dtuPayTransaction.setTime(transaction.getTime());
        return dtuPayTransaction;
    }
}
