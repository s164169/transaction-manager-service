package com.dtupay.transactionmanager.Converters;

import com.dtupay.transactionmanager.Models.DTUPayAccount;
import dtu.ws.fastmoney.Account;

import java.util.stream.Collectors;

//@author William Embarek 
public class ConvertAccount {

    public static DTUPayAccount convertAccount(Account account) {
        DTUPayAccount dtuPayAccount = new DTUPayAccount();
        dtuPayAccount.setBalance(account.getBalance());
        dtuPayAccount.setId(account.getId());
        dtuPayAccount.setUser(ConvertUser.revertUser(account.getUser()));
        dtuPayAccount.setTransactions(account.getTransactions().stream().map(ConvertTransaction::convertTransaction).collect(Collectors.toList()));
        return dtuPayAccount;
    }
}
