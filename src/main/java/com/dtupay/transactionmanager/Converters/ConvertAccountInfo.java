package com.dtupay.transactionmanager.Converters;

import com.dtupay.transactionmanager.Models.DTUPayAccountInfo;
import dtu.ws.fastmoney.AccountInfo;
//@author Magnus Glasdam Jakobsen
public class ConvertAccountInfo {
    public static DTUPayAccountInfo convertAccountInfo(AccountInfo accountInfo) {
        DTUPayAccountInfo dtuPayAccountInfo = new DTUPayAccountInfo();
        dtuPayAccountInfo.setAccountId(accountInfo.getAccountId());
        dtuPayAccountInfo.setUser(ConvertUser.revertUser(accountInfo.getUser()));
        return dtuPayAccountInfo;
    }
}
