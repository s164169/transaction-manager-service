
package com.dtupay.transactionmanager.Models;
//@author Mathias Boss Jørgensen
public class DTUPayAccountInfo {

    protected String accountId;
    protected DTUPayUser user;

    /**
     * Gets the value of the accountId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the user property.
     *
     * @return
     *     possible object is
     *     {@link dtu.ws.fastmoney.User }
     *
     */
    public DTUPayUser getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     *
     * @param value
     *     allowed object is
     *     {@link dtu.ws.fastmoney.User }
     *
     */
    public void setUser(DTUPayUser value) {
        this.user = value;
    }

}
