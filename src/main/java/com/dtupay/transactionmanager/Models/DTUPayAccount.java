
package com.dtupay.transactionmanager.Models;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
//@author Mathias Bo Jensen
public class DTUPayAccount {

    protected BigDecimal balance;
    protected String id;
    protected List<DTUPayTransaction> transactions;
    protected DTUPayUser DTUPayUser;

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalance(BigDecimal value) {
        this.balance = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }
    
    public List<DTUPayTransaction> getTransactions() {
        if (transactions == null) {
            transactions = new ArrayList<DTUPayTransaction>();
        }
        return this.transactions;
    }

    /**
     * Gets the value of the DTUPayUser property.
     * 
     * @return
     *     possible object is
     *     {@link DTUPayUser }
     *     
     */
    public DTUPayUser getUser() {
        return DTUPayUser;
    }

    /**
     * Sets the value of the DTUPayUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTUPayUser }
     *     
     */
    public void setUser(DTUPayUser value) {
        this.DTUPayUser = value;
    }

    public void setTransactions(List<DTUPayTransaction> transactions) {
        this.transactions = transactions;
    }

}
