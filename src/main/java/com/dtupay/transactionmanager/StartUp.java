package com.dtupay.transactionmanager;

import com.dtupay.transactionmanager.Abstractions.ITransactionRequestRepository;
import com.dtupay.transactionmanager.Adapters.SoapBankAdapter;
import com.dtupay.transactionmanager.Recievers.TransactionManagerServiceReceiver;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.DAL.models.Token;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

import java.math.BigDecimal;
import java.util.UUID;
//@author Mathias Bo Jensen
public class StartUp {

    public static void main(String[] args) throws Exception{
        new StartUp().startUp();
    }

    private void startUp() throws Exception {

        ITransactionRequestRepository inMemoryRepository = new InMemoryRepository();
        EventSender eventSender = new RabbitMqSender();
        SoapBankAdapter soapBankReceiver = new SoapBankAdapter();


        TransactionManagerService transactionManagerService = new TransactionManagerService(soapBankReceiver, inMemoryRepository);
        TransactionManagerServiceReceiver transactionManagerServiceReceiver = new TransactionManagerServiceReceiver(transactionManagerService, eventSender);
        new RabbitMqListener(transactionManagerServiceReceiver).listen();
    }
}
