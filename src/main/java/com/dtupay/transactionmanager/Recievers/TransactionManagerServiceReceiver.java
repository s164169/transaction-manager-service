package com.dtupay.transactionmanager.Recievers;

import com.dtupay.transactionmanager.Abstractions.ITransactionManagerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
//@author Frederik Kirkegaard
public class TransactionManagerServiceReceiver implements EventReceiver {
    private EventSender sender;
    private ITransactionManagerService transactionManagerService;

    //New version
    private static final String TRANSFER_MONEY_FROM_TO_VALIDATED = "transferMoneyFromToValidated";
    private static final String TRANSFER_MONEY_FROM_TO_SUCCEEDED = "transferMoneyFromToSucceeded";

    private static final String DELETE_CUSTOMER = "DeleteCustomer";
    private static final String CREATE_CUSTOMER = "CreateCustomer";
    private static final String CREATE_MERCHANT = "CreateMerchant";
    private static final String DELETE_MERCHANT = "DeleteMerchant";
    private static final String UPDATE_CUSTOMER = "UpdateCustomer";
    private static final String UPDATE_MERCHANT = "UpdateMerchant";

    private static final String REQUEST_TOKENS_SUCCEEDED = "RequestTokensSucceeded";
    private static final String REFUND_PAYMENT_VALIDATED = "RefundPaymentValidated";
    private static final String REFUND_PAYMENT_SUCCEEDED = "RefundPaymentSucceeded";

    //Reporting
    private static final String GET_TRANSACTION_REQUESTS_BY_MERCHANT_ID_REPORTING = "GetTransactionRequestsByMerchantIDReporting";
    private static final String GET_TRANSACTION_REQUESTS_BY_MERCHANT_ID_REPORTING_SUCCEEDED= "GetTransactionRequestsByMerchantIDReporting";

    public TransactionManagerServiceReceiver(ITransactionManagerService transactionManagerService, EventSender sender) {
        this.transactionManagerService = transactionManagerService;
        this.sender = sender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        Gson gson = new Gson();
        TransactionRequest transactionRequest;
        Object[] args = event.getArguments();
        switch (event.getEventType()) {

            //New version
            case TRANSFER_MONEY_FROM_TO_VALIDATED:
                System.out.println("Handling event: " + event);
                PaymentDTO paymentDTO = gson.fromJson(gson.toJson(args[0]), PaymentDTO.class);
                TransferMoneyDTO transferMoneyDTO = transactionManagerService.transferMoney(paymentDTO);
                sendEvent(TRANSFER_MONEY_FROM_TO_SUCCEEDED, new Object[] {transferMoneyDTO});
                break;

            case CREATE_CUSTOMER:
                System.out.println("Handling event: " + event);
                transactionManagerService.addCustomer(gson.fromJson(gson.toJson(args[0]), Customer.class));
                break;

            case UPDATE_CUSTOMER:
                System.out.println("Handling event: " + event);
                transactionManagerService.updateCustomer(gson.fromJson(gson.toJson(args[0]), Customer.class));
                break;

            case DELETE_CUSTOMER:
                System.out.println("Handling event: " + event);
                transactionManagerService.deleteCustomer(gson.fromJson(gson.toJson(args[0]), UUID.class));
                break;

            case CREATE_MERCHANT:
                System.out.println("Handling event: " + event);
                transactionManagerService.addMerchant(gson.fromJson(gson.toJson(args[0]), Merchant.class));
                break;

            case UPDATE_MERCHANT:
                System.out.println("Handling event: " + event);
                transactionManagerService.updateMerchant(gson.fromJson(gson.toJson(args[0]), Merchant.class));
                break;

            case DELETE_MERCHANT:
                System.out.println("Handling event: " + event);
                transactionManagerService.deleteMerchant(gson.fromJson(gson.toJson(args[0]), UUID.class));
                break;


            case REQUEST_TOKENS_SUCCEEDED:
                System.out.println("Handling event: " + event);
                Type listType = new TypeToken<ArrayList<Token>>(){}.getType();
                List<Token> listOfTokens = gson.fromJson(gson.toJson(args[0]), listType);
                transactionManagerService.addTokens(listOfTokens);
                break;

            case REFUND_PAYMENT_VALIDATED:
                System.out.println("Handling event: " + event);
                PaymentDTO payDTO = gson.fromJson(gson.toJson(args[0]), PaymentDTO.class);
                TransferMoneyDTO transferDTO = transactionManagerService.refundPayment(payDTO);
                sendEvent(REFUND_PAYMENT_SUCCEEDED, new Object[] {transferDTO});
                break;

            default:
                System.out.println("Ignoring event " + event);
                break;
        }
    }

    private void sendEvent(String event, Object[] receiveEventResult) throws Exception {
        Event e = new Event(event, receiveEventResult);
        sender.sendEvent(e);
    }
}
