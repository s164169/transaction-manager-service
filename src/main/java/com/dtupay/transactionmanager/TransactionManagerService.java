package com.dtupay.transactionmanager;


import com.dtupay.transactionmanager.Abstractions.*;
import com.dtupay.transactionmanager.Models.*;
import dtu.ws.fastmoney.Account;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
//@author Mathias Boss Jørgensen
//TODO: Tilføj/refactor TransactionRequestRepository
public class TransactionManagerService implements ITransactionManagerService {
    private IBank bank;
    private ITransactionRequestRepository transactionRequestRepository;

    public TransactionManagerService(IBank bank,
                                     ITransactionRequestRepository transactionRequestRepository) {
        this.bank = bank;
        this.transactionRequestRepository = transactionRequestRepository;
    }

    @Override
    public List<TransactionRequest> getTransactionRequestsByMerchantID(UUID merchantID) {
        return transactionRequestRepository.getAllByMerchantID(merchantID);
    }

    @Override
    public TransactionRequest generateTransaction(Token customerToken, Merchant merchant, BigDecimal amount, Customer customer, String description) {
        TransactionRequest transactionRequest = new TransactionRequest(UUID.randomUUID(), customerToken, merchant, amount, customer, description);
        transactionRequestRepository.addTransaction(transactionRequest);
        return transactionRequest;
    }

    private void executeTransaction(TransactionRequest transactionRequest, DTUPayAccount fromAccount, DTUPayAccount toAccount) throws Exception {
        BigDecimal amount = transactionRequest.getAmount();
        String description = transactionRequest.getDescription();
        bank.transferMoneyFromTo(fromAccount.getId(), toAccount.getId(), amount, description);
        transactionRequest.setTransactionStatus(TransactionRequest.TransactionStatus.SUCCEEDED);
    }

    @Override
    public void processTransaction(TransactionRequest transactionRequest) throws Exception {

            DTUPayAccount merchantAccount = bank.getAccountByCPRNumber(transactionRequest.getMerchant().getCprNumber());
            DTUPayAccount customerAccount = bank.getAccountByCPRNumber(transactionRequest.getCustomer().getCprNumber());
            executeTransaction(transactionRequest, customerAccount, merchantAccount);
            updateEntitiesAfterTransfer(transactionRequest);

    }

    @Override
    public void processRefund(TransactionRequest transactionRequest) throws Exception {
            DTUPayAccount merchantAccount = bank.getAccountByCPRNumber(transactionRequest.getMerchant().getCprNumber());
            DTUPayAccount customerAccount = bank.getAccountByCPRNumber(transactionRequest.getCustomer().getCprNumber());
            executeTransaction(transactionRequest, merchantAccount, customerAccount);
            updateEntitiesAfterTransfer(transactionRequest);

    }

    private void updateEntitiesAfterTransfer(TransactionRequest transactionRequest) {
        transactionRequestRepository.updateTransactionRequest(transactionRequest);
        Merchant merchant = transactionRequest.getMerchant();
        List<UUID> transactionRequests = merchant.getTransactionRequestIDs();
        transactionRequests.add(transactionRequest.getTransactionRequestID());
        merchant.setTransactionRequestIDs(transactionRequests);

        Customer customer = transactionRequest.getCustomer();
        List<UUID> transactionRequestsCustomer = customer.getTransactionRequestIDs();
        transactionRequestsCustomer.add(transactionRequest.getTransactionRequestID());
        customer.setTransactionRequestIDs(transactionRequestsCustomer);

        //TODO: LAV UPDATE CALL
    }

    @Override
    public boolean isTransactionSucceeded(String transactionRequestID) {
        return TransactionRequest.TransactionStatus.SUCCEEDED == transactionRequestRepository.getTransactionRequest(UUID.fromString(transactionRequestID)).getTransactionStatus();
    }

    @Override
    public TransferMoneyDTO transferMoney(PaymentDTO paymentDTO) throws Exception {
        Merchant merchant = transactionRequestRepository.getMerchantByID(paymentDTO.merchanID);
        Customer customer = transactionRequestRepository.getCustomerByTokenID(paymentDTO.tokenID);
        DTUPayAccount accountMerchant = bank.getAccountByCPRNumber(merchant.getCprNumber());
        DTUPayAccount accountCustomer = bank.getAccountByCPRNumber(customer.getCprNumber());

        bank.transferMoneyFromTo(accountCustomer.getId(), accountMerchant.getId(), paymentDTO.amount, paymentDTO.description);

        TransferMoneyDTO transferMoneyDTO = new TransferMoneyDTO();
        transferMoneyDTO.setTokenID(paymentDTO.tokenID);
        transferMoneyDTO.setAmount(paymentDTO.amount);
        transferMoneyDTO.setDescription(paymentDTO.description);
        transferMoneyDTO.setFromAccount(accountCustomer.getId());
        transferMoneyDTO.setToAccount(accountMerchant.getId());

        return transferMoneyDTO;

    }

    @Override
    public void addCustomer(Customer customer) {
        transactionRequestRepository.addCustomer(customer);
    }

    @Override
    public void deleteCustomer(UUID customerID) {
        transactionRequestRepository.deleteCustomer(customerID);    }

    @Override
    public void updateCustomer(Customer customer) {
        transactionRequestRepository.updateCustomer(customer);    }

    @Override
    public void addMerchant(Merchant merchant) {
        transactionRequestRepository.addMerchant(merchant);    }

    @Override
    public void deleteMerchant(UUID merchantID) {
        transactionRequestRepository.deleteMerchant(merchantID);    }

    @Override
    public void updateMerchant(Merchant merchant) {
        transactionRequestRepository.updateMerchant(merchant);    }

    @Override
    public TransferMoneyDTO refundPayment(PaymentDTO payDTO) throws Exception {
        Merchant merchant = transactionRequestRepository.getMerchantByID(payDTO.merchanID);
        Customer customer = transactionRequestRepository.getCustomerByTokenID(payDTO.tokenID);
        DTUPayAccount accountMerchant = bank.getAccountByCPRNumber(merchant.getCprNumber());
        DTUPayAccount accountCustomer = bank.getAccountByCPRNumber(customer.getCprNumber());

        bank.transferMoneyFromTo(accountMerchant.getId(), accountCustomer.getId(), payDTO.amount, payDTO.description);

        TransferMoneyDTO transferMoneyDTO = new TransferMoneyDTO();
        transferMoneyDTO.setTokenID(payDTO.tokenID);
        transferMoneyDTO.setAmount(payDTO.amount);
        transferMoneyDTO.setDescription(payDTO.description);
        transferMoneyDTO.setFromAccount(accountMerchant.getId());
        transferMoneyDTO.setToAccount(accountCustomer.getId());

        return transferMoneyDTO;

    }

    @Override
    public void addTokens(List<Token> listOfTokens) {
        transactionRequestRepository.addTokens(listOfTokens);
    }
}