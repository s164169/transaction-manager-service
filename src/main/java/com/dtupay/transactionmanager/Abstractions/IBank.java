package com.dtupay.transactionmanager.Abstractions;

import com.dtupay.transactionmanager.Models.DTUPayAccount;
import com.dtupay.transactionmanager.Models.DTUPayAccountInfo;
import com.dtupay.transactionmanager.Models.DTUPayUser;

import java.math.BigDecimal;
//@author Frederik Kirkegaard
public interface IBank {

    public String createAccountWithBalance(DTUPayUser dtuPayUser, BigDecimal initialBalance) throws Exception;

    public DTUPayAccount getAccount(String accountNumber) throws Exception;

    public DTUPayAccount getAccountByCPRNumber(String cpr) throws Exception;

    public DTUPayAccountInfo[] getAccounts() throws Exception;

    public void retireAccount(String accountNumber) throws Exception;

    public void transferMoneyFromTo(String accountFrom, String accountTo, BigDecimal amount, String description) throws Exception;

}
