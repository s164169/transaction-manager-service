package com.dtupay.transactionmanager.Abstractions;


import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;

import java.util.List;
import java.util.UUID;
//@author Mathias Bo Jensen
public interface ITransactionRequestRepository {
    void addTransaction(TransactionRequest transactionRequest);

    TransactionRequest getTransactionRequest(UUID transactionRequestID);

    List<TransactionRequest> getAllByMerchantID(UUID merchantID);

    void updateTransactionRequest(TransactionRequest transactionRequest);

    Merchant getMerchantByID(UUID merchanID);

    Customer getCustomerByTokenID(String tokenID);

    void addCustomer(Customer customer);
    void deleteCustomer(UUID customerID);
    void updateCustomer(Customer customer);

    void addMerchant(Merchant merchant);
    void deleteMerchant(UUID merchantID);
    void updateMerchant(Merchant merchant);

    public void addTokens(List<Token> listOfTokens);

    void addToken(Token token);
}
