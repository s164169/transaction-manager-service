package com.dtupay.transactionmanager.Abstractions;

import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
//@author Sebatian Fischer
public interface ITransactionManagerService {
    List<TransactionRequest> getTransactionRequestsByMerchantID(UUID merchantID);

    TransactionRequest generateTransaction(Token customerToken, Merchant merchant, BigDecimal amount, Customer customer, String description);

    void processTransaction(TransactionRequest transactionRequest) throws Exception;

    void processRefund(TransactionRequest transactionRequest) throws Exception;

    boolean isTransactionSucceeded(String transactionRequestID);

    TransferMoneyDTO transferMoney(PaymentDTO paymentDTO) throws Exception;
    void addTokens(List<Token> listOfTokens);

    void addCustomer(Customer customer);
    void deleteCustomer(UUID customerID);
    void updateCustomer(Customer customer);

    void addMerchant(Merchant merchant);
    void deleteMerchant(UUID merchantID);
    void updateMerchant(Merchant merchant);

    TransferMoneyDTO refundPayment(PaymentDTO payDTO) throws Exception;
}
