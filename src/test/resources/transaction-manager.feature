#//@author Frederik Kirkegaard
Feature: Transaction Manager

  Scenario: Transfer Money Validated
    Given A payment DTO
    When the service receives a "transferMoneyFromToValidated" event
    Then the money is transfered
    Then the "transferMoneyFromToSucceeded" event is broadcast

  Scenario: Update Customer
    Given a customer
    When the service receives a "UpdateCustomer" customer event
    Then the customer is updated

  Scenario: Update merchant
    Given a merchant
    When the service receives a "UpdateMerchant" merchant event
    Then the merchant is updated

  Scenario: Delete Customer
    Given a customer
    When the service receives a "DeleteCustomer" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then the Customer is deleted

  Scenario: Delete merchant
    Given a merchant
    When the service receives a "DeleteMerchant" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then the merchant is deleted

  Scenario: Create merchant
    Given a merchant
    When the service receives a "CreateMerchant" merchant event
    Then the merchant is created

  Scenario: Create customer
    Given a customer
    When the service receives a "CreateCustomer" customer event
    Then the customer is created

  Scenario: Request Tokens success
    Given a token list
    When the service receives a "RequestTokensSucceeded" tokens event
    Then the tokens are added

  Scenario: Refund Succeeded
    Given A payment DTO
    When the service receives a "RefundPaymentValidated" payment event
    Then the payment is refunded
    Then the "RefundPaymentSucceeded" event is broadcast



