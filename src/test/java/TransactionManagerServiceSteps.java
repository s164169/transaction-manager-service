import com.dtupay.transactionmanager.Abstractions.IBank;
import com.dtupay.transactionmanager.Abstractions.ITransactionManagerService;
import com.dtupay.transactionmanager.Models.DTUPayUser;
import com.dtupay.transactionmanager.Recievers.TransactionManagerServiceReceiver;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;
import messaging.Event;
import messaging.EventSender;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
//@author Magnus Glasdam Jakobsen
public class TransactionManagerServiceSteps {

    ITransactionManagerService transactionManager = mock(ITransactionManagerService.class);
    IBank soapBankReceiver = mock(IBank.class);
    EventSender eventSender = mock(EventSender.class);

    private TransactionManagerServiceReceiver transactionManagerServiceReceiver = new TransactionManagerServiceReceiver(transactionManager,eventSender);


    private Merchant merchant;
    private Customer customer;
    private PaymentDTO paymentDTO;
    private List<Token> tokenList;
    private TransferMoneyDTO transferMoneyDTO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Given("^A payment DTO$")
    public void aPaymentDTO() throws Exception {
        paymentDTO = new PaymentDTO();
        transferMoneyDTO = new TransferMoneyDTO();
        doReturn(transferMoneyDTO).when(transactionManager).transferMoney(any(PaymentDTO.class));
        doReturn(transferMoneyDTO).when(transactionManager).refundPayment(any(PaymentDTO.class));
    }

    @When("^the service receives a \"([^\"]*)\" event$")
    public void theServiceReceivesAEvent(String arg0) throws Throwable {
        Object[] args =  {paymentDTO} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the money is transfered$")
    public void theMoneyIsTransfered() throws Exception {
        verify(transactionManager).transferMoney(any(PaymentDTO.class));
    }

    @Then("^the \"([^\"]*)\" event is broadcast$")
    public void theEventIsBroadcast(String eventType) throws Throwable {
        Object[] args = {transferMoneyDTO};
        Event event = new Event(eventType, args);
        verify(eventSender).sendEvent(refEq(event));
    }

    @When("^the service receives a \"([^\"]*)\" customer event$")
    public void theServiceReceivesACustomerEvent(String arg0) throws Throwable {
        Object[] args =  {customer} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the customer is updated$")
    public void theCustomerIsUpdated() {
        verify(transactionManager).updateCustomer(any(Customer.class));
    }

    @Given("^a merchant$")
    public void aMerchant() {
        merchant = new Merchant("test1","test1","test1",UUID.randomUUID(),"test1");
        doNothing().when(transactionManager).addMerchant(any(Merchant.class));
        doNothing().when(transactionManager).updateMerchant(any(Merchant.class));
    }


    @Given("^a customer$")
    public void aCustomer() {
        customer = new Customer("test1","test1","test1",UUID.randomUUID(),"test1");
        doNothing().when(transactionManager).addCustomer(any(Customer.class));
        doNothing().when(transactionManager).updateCustomer(any(Customer.class));
    }

    @When("^the service receives a \"([^\"]*)\" merchant event$")
    public void theServiceReceivesAMerchantEvent(String arg0) throws Throwable {
        Object[] args =  {merchant} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the merchant is updated$")
    public void theMerchantIsUpdated() {
        verify(transactionManager).updateMerchant(any(Merchant.class));
    }

    @When("^the service receives a \"([^\"]*)\" event with ID \"([^\"]*)\"$")
    public void theServiceReceivesAEventWithID(String arg0, String arg1) throws Throwable {
        Object[] args =  {UUID.fromString(arg1)} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);

    }

    @Then("^the Customer is deleted$")
    public void theCustomerIsDeleted() {
        verify(transactionManager).deleteCustomer(any(UUID.class));
    }

    @Then("^the merchant is deleted$")
    public void theMerchantIsDeleted() {
        verify(transactionManager).deleteMerchant(any(UUID.class));
    }

    @Then("^the merchant is created$")
    public void theMerchantIsCreated() {
        verify(transactionManager).addMerchant(any(Merchant.class));
    }

    @Then("^the customer is created$")
    public void theCustomerIsCreated() {
        verify(transactionManager).addCustomer(any(Customer.class));
    }

    @Given("^a token list$")
    public void aTokenList() throws Exception {
        tokenList = new ArrayList<Token>();
        doNothing().when(transactionManager).addTokens(anyListOf(Token.class));
    }

    @When("^the service receives a \"([^\"]*)\" tokens event$")
    public void theServiceReceivesATokensEvent(String arg0) throws Throwable {
        Object[] args = {tokenList} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the tokens are added$")
    public void theTokensAreAdded() {
        verify(transactionManager).addTokens(anyListOf(Token.class));
    }

    @When("^the service receives a \"([^\"]*)\" payment event$")
    public void theServiceReceivesAPaymentEvent(String arg0) throws Throwable {
        Object[] args = {paymentDTO} ;
        Event event = new Event(arg0, args);
        transactionManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the payment is refunded$")
    public void thePaymentIsRefunded() throws Exception {
        verify(transactionManager).refundPayment(any(PaymentDTO.class));
    }
}
